package API;

import io.restassured.http.ContentType;
import org.junit.FixMethodOrder;
import org.junit.Test;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class methodValidation {

    @Test
    public void Test_01_CreateUserSuccessfully() {      // Cenário: Cadastrar usuário com sucesso;
        // Caminho para API
        baseURI = "https://reqres.in/api/users";

        // Create User com sucesso

        given()
                .body("{\n" +
                        "\"name\": \"Francisco\",\n" +
                        "\"job\": \"Quality Assurance\"\n" +
                        "}")
                .contentType(ContentType.JSON)
            .when()
                .post(baseURI)
            .then()
                .log().all()
                .assertThat()
                    .statusCode(201)
                    .body(containsString("Francisco"));
        }

    @Test
    public void Test_02_SearchOneUser() {               // Cenário: Pesquisar por apenas um usuário;
        // Caminho para API

        baseURI = "https://reqres.in/api/users/";

        // Pesquisar por apenas um usuário

        given()
                .contentType(ContentType.JSON)
            .when()
                .get(baseURI + "2")
            .then()
                .log().all()
                .assertThat()
                    .statusCode(200)
                        .body("data.id", equalTo(2));
    }

    @Test
    public void Test_03_SearchListUsers(){               // Cenário: Pesquisar por lista de usuários;
        // Caminho para API

        baseURI = "https://reqres.in/api/users/";

        // Pesquisar por todos os usuários da página 2

        given().
                param("page", "2").
            when().
                get(baseURI).
            then().
                log().all().
                    statusCode(200).
                        body("page", equalTo(2));
    }
    @Test
    public void Test_04_UpdateData(){                   // Cenário: Recolher dadas de atualização de usuário;
        // Caminho para API

        baseURI = "https://reqres.in/api/users/";

        // Atualizar dados

        given().
                body("{\n" +
                        "\"name\": \"Francisco\",\n" +
                        "\"job\": \"Quality Assurance\"\n" +
                        "}").
            when().
                patch(baseURI + "2").
            then().
                log().all().
                    statusCode(200);
    }
}
