package Web.resources;

import com.github.javafaker.Faker;

public class variables {

    /////////
    //Setup//
    /////////

    private String arquivoChromeDriver = "/home/franciscojunior/IdeaProjects/chromedriver";
    private String baseURL = "http://automationpractice.com/index.php";

    ////////////
    //Purchase//
    ////////////

    // ---- ID ---- //

    private String idHoveredPrinted = "//*[@id=\"homefeatured\"]/li[7]";
    private String idMorePrinted = "//*[@id=\"homefeatured\"]/li[7]/div/div[2]/div[2]/a[2]";
    private String idUniformGroup = "uniform-group_1";
    private String idSizeM = "//*[@id=\"group_1\"]/option[2]";
    private String idGreenColor = "color_15";
    private String idAddToCard = "add_to_cart";
    private String idContinueShopping = "//*[@id=\"layer_cart\"]/div[1]/div[2]/div[4]/span";
    private String idIconHome = "//*[@id=\"columns\"]/div[1]/a[1]/i";
    private String idHoveredFaded = "//*[@id=\"homefeatured\"]/li[1]";
    private String idMoreFaded = "//*[@id=\"homefeatured\"]/li[1]/div/div[2]/div[2]/a[2]";
    private String idBlueColor = "color_14";
    private String idHoveredBlouse = "//*[@id=\"homefeatured\"]/li[2]";
    private String idMoreBlouse = "//*[@id=\"homefeatured\"]/li[2]/div/div[2]/div[2]/a[2]";
    private String idQuantityWanted = "//*[@id=\"quantity_wanted_p\"]/a[2]";
    private String idCard = "//*[@id=\"header\"]/div[3]/div/div/div[3]/div/a";
    private String idButtonOrderCard = "button_order_cart";
    private String idButtonProceedToCheckout1 = "//*[@id=\"center_column\"]/p[2]/a[1]";
    private String idEmail = "email";
    private String idSubmitLogin = "SubmitLogin";
    private String idButtonProceedToCheckout3 = "//*[@id=\"center_column\"]/form/p/button";
    private String idCheckTermOfService = "uniform-cgv";
    private String idButtonProceedToCheckout4 = "//*[@id=\"form\"]/p/button";
    private String idPaymentType1 = "//*[@id=\"HOOK_PAYMENT\"]/div[2]/div/p/a";
    private String idConfirmMyOrder = "//*[@id=\"cart_navigation\"]/button";
    private String idQuickViewFaded = "//*[@id=\"homefeatured\"]/li[1]/div/div[1]/div/a[2]";
    private String idQuickViewBlouse = "//*[@id=\"homefeatured\"]/li[2]/div/div[1]/div/a[2]";
    private String idQuickViewPrinted = "//*[@id=\"homefeatured\"]/li[7]/div/div[1]/div/a[2]";
    private String idIframe = "//iframe[@class=\"fancybox-iframe\"]";
    private String idCategoryDress = "//*[@id=\"block_top_menu\"]/ul/li[2]";
    private String idSummerDress = "//*[@id=\"block_top_menu\"]/ul/li[2]/ul/li[3]";
    private String idCategoryHoveredPrinted = "//*[@id=\"center_column\"]/ul/li[3]/div/div[2]/h5";
    private String idCategoryMorePrinted = "//*[@id=\"center_column\"]/ul/li[3]/div/div[2]/div[2]/a[2]";
    private String idCategoryWomen = "//*[@id=\"block_top_menu\"]/ul/li[1]";
    private String idSelectTshirt = "//*[@id=\"block_top_menu\"]/ul/li[1]/ul/li[1]/ul/li[1]/a";
    private String idCategoryHoveredFaded = "//*[@id=\"center_column\"]/ul/li/div/div[2]/h5";
    private String idHoveredColorFaded = "color_2";
    private String idSelectBlouse = "//*[@id=\"block_top_menu\"]/ul/li[1]/ul/li[1]/ul/li[2]/a";
    private String idCategoryHoveredBlouse = "//*[@id=\"center_column\"]/ul/li/div/div[2]/h5";
    private String idCategoryQuickViewBlouse = "//*[@id=\"center_column\"]/ul/li/div/div[1]/div/a[2]";
    private String idEveningDress = "//*[@id=\"block_top_menu\"]/ul/li[2]/ul/li[2]";
    private String idCategoryHoveredPrinted2 = "//*[@id=\"center_column\"]/ul/li";
    private String idQuickAddToCardPrinted2 = "//*[@id=\"center_column\"]/ul/li/div/div[2]/div[2]/a[1]";


    //////////
    //SignUp//
    //////////

    // ---- ID ---- //
    private String idButtonSignUp = "//*[@id=\"header\"]/div[2]/div/div/nav/div[1]/a";

    private String idEmailCreate = "email_create";
    private String idSubmitCreate = "SubmitCreate";
    private String idGender = "id_gender1";
    private String idCustomerFirstname = "customer_firstname";
    private String idCustomerLastname = "customer_lastname";
    private String idPasswd = "passwd";
    private String idDays = "days";
    private String idMonths = "months";
    private String idYears = "years";
    private String idNewsletter = "newsletter";
    private String idOptin = "optin";
    private String idAddressFirstname = "firstname";
    private String idAddressLastname = "lastname";
    private String idCompany = "company";
    private String idAddress1 = "address1";
    private String idAddress2 = "address2";
    private String idCity = "city";
    private String idState = "id_state";
    private String idPostcode = "postcode";
    private String idCountry = "id_country";
    private String idOther = "other";
    private String idPhone = "phone";
    private String idPhoneMobile = "phone_mobile";
    private String idAlias = "alias";
    private String idSubmitAccount = "submitAccount";
    private String nameValidationSuccessfullySignUp = "My account";

    ////////////
    //JavaFake//
    ////////////
    Faker faker = new Faker();

    private String fakePhoneNumber = faker.phoneNumber().cellPhone();
    private String fakeFirstName = faker.name().firstName();
    private String fakeLastName = faker.name().lastName();

    ////////
    //Text//
    ////////

    // --- Purchase --- //

    private String defultTitleHomePage = "Automation Practice Website";
    private String color01 = "Blue";
    private String quantityWanted = "2";
    private String validation1 = "Color : Green, Size : M";
    private String validation2 = "Color : Blue";
    private String validation3 = "2";
    private String validation4 = "Your shopping cart contains: 4 Products";
    private String validation5 = "Green, M";
    private String validation6 = "Your shopping cart contains: 5 Products";
    private String defaultEmail = "francisco@me.com";
    private String defaultPassword = "12345678";
    private String defaultMensageOrderCompleted = "Your order on My Store is complete.";


    // --- SignUp --- //
    private String defaulPassword = "12345678";

    private String defaultDay = "8";
    private String defaultMonth = "8";
    private String defaultYear = "2000";
    private String defaultCompany = "Renner";
    private String defaultAddress1 = "483  Hood Avenue, 92128";
    private String defaultAddress2 = "Apartment 4, unit 1";
    private String defaultCity = "San Francisco";
    private String defaultState = "5";
    private String defaultPostcode = "94110";
    private String defaultCountry = "21";
    private String defaultOther = "N/A";
    private String defaultPhone = "209-200-4422";








    // --- encapsulate --- //
    public String getArquivoChromeDriver() {
        return arquivoChromeDriver;
    }

    public void setArquivoChromeDriver(String arquivoChromeDriver) {
        this.arquivoChromeDriver = arquivoChromeDriver;
    }

    public String getBaseURL() {
        return baseURL;
    }

    public void setBaseURL(String baseURL) {
        this.baseURL = baseURL;
    }

    public String getIdButtonSignUp() {
        return idButtonSignUp;
    }

    public void setIdButtonSignUp(String idButtonSignUp) {
        this.idButtonSignUp = idButtonSignUp;
    }

    public String getIdEmailCreate() {
        return idEmailCreate;
    }

    public void setIdEmailCreate(String idEmailCreate) {
        this.idEmailCreate = idEmailCreate;
    }

    public String getIdSubmitCreate() {
        return idSubmitCreate;
    }

    public void setIdSubmitCreate(String idSubmitCreate) {
        this.idSubmitCreate = idSubmitCreate;
    }

    public String getIdGender() {
        return idGender;
    }

    public void setIdGender(String idGender) {
        this.idGender = idGender;
    }

    public String getIdCustomerFirstname() {
        return idCustomerFirstname;
    }

    public void setIdCustomerFirstname(String idCustomerFirstname) {
        this.idCustomerFirstname = idCustomerFirstname;
    }

    public String getIdCustomerLastname() {
        return idCustomerLastname;
    }

    public void setIdCustomerLastname(String idCustomerLastname) {
        this.idCustomerLastname = idCustomerLastname;
    }

    public String getIdPasswd() {
        return idPasswd;
    }

    public void setIdPasswd(String idPasswd) {
        this.idPasswd = idPasswd;
    }

    public String getIdDays() {
        return idDays;
    }

    public void setIdDays(String idDays) {
        this.idDays = idDays;
    }

    public String getIdMonths() {
        return idMonths;
    }

    public void setIdMonths(String idMonths) {
        this.idMonths = idMonths;
    }

    public String getIdYears() {
        return idYears;
    }

    public void setIdYears(String idYears) {
        this.idYears = idYears;
    }

    public String getIdNewsletter() {
        return idNewsletter;
    }

    public void setIdNewsletter(String idNewsletter) {
        this.idNewsletter = idNewsletter;
    }

    public String getIdOptin() {
        return idOptin;
    }

    public void setIdOptin(String idOptin) {
        this.idOptin = idOptin;
    }

    public String getIdAddressFirstname() {
        return idAddressFirstname;
    }

    public void setIdAddressFirstname(String idAddressFirstname) {
        this.idAddressFirstname = idAddressFirstname;
    }

    public String getIdAddressLastname() {
        return idAddressLastname;
    }

    public void setIdAddressLastname(String idAddressLastname) {
        this.idAddressLastname = idAddressLastname;
    }

    public String getIdCompany() {
        return idCompany;
    }

    public void setIdCompany(String idCompany) {
        this.idCompany = idCompany;
    }

    public String getIdAddress1() {
        return idAddress1;
    }

    public void setIdAddress1(String idAddress1) {
        this.idAddress1 = idAddress1;
    }

    public String getIdAddress2() {
        return idAddress2;
    }

    public void setIdAddress2(String idAddress2) {
        this.idAddress2 = idAddress2;
    }

    public String getIdCity() {
        return idCity;
    }

    public void setIdCity(String idCity) {
        this.idCity = idCity;
    }

    public String getIdState() {
        return idState;
    }

    public void setIdState(String idState) {
        this.idState = idState;
    }

    public String getIdPostcode() {
        return idPostcode;
    }

    public void setIdPostcode(String idPostcode) {
        this.idPostcode = idPostcode;
    }

    public String getIdCountry() {
        return idCountry;
    }

    public void setIdCountry(String idCountry) {
        this.idCountry = idCountry;
    }

    public String getIdOther() {
        return idOther;
    }

    public void setIdOther(String idOther) {
        this.idOther = idOther;
    }

    public String getIdPhone() {
        return idPhone;
    }

    public void setIdPhone(String idPhone) {
        this.idPhone = idPhone;
    }

    public String getIdPhoneMobile() {
        return idPhoneMobile;
    }

    public void setIdPhoneMobile(String idPhoneMobile) {
        this.idPhoneMobile = idPhoneMobile;
    }

    public String getIdAlias() {
        return idAlias;
    }

    public void setIdAlias(String idAlias) {
        this.idAlias = idAlias;
    }

    public String getIdSubmitAccount() {
        return idSubmitAccount;
    }

    public void setIdSubmitAccount(String idSubmitAccount) {
        this.idSubmitAccount = idSubmitAccount;
    }

    public String getFakePhoneNumber() {
        return fakePhoneNumber;
    }

    public void setFakePhoneNumber(String fakePhoneNumber) {
        this.fakePhoneNumber = fakePhoneNumber;
    }

    public String getFakeFirstName() {
        return fakeFirstName;
    }

    public void setFakeFirstName(String fakeFirstName) {
        this.fakeFirstName = fakeFirstName;
    }

    public String getFakeLastName() {
        return fakeLastName;
    }

    public void setFakeLastName(String fakeLastName) {
        this.fakeLastName = fakeLastName;
    }

    public String getDefaulPassword() {
        return defaulPassword;
    }

    public void setDefaulPassword(String defaulPassword) {
        this.defaulPassword = defaulPassword;
    }

    public String getDefaultDay() {
        return defaultDay;
    }

    public void setDefaultDay(String defaultDay) {
        this.defaultDay = defaultDay;
    }

    public String getDefaultMonth() {
        return defaultMonth;
    }

    public void setDefaultMonth(String defaultMonth) {
        this.defaultMonth = defaultMonth;
    }

    public String getDefaultYear() {
        return defaultYear;
    }

    public void setDefaultYear(String defaultYear) {
        this.defaultYear = defaultYear;
    }

    public String getDefaultCompany() {
        return defaultCompany;
    }

    public void setDefaultCompany(String defaultCompany) {
        this.defaultCompany = defaultCompany;
    }

    public String getDefaultAddress1() {
        return defaultAddress1;
    }

    public void setDefaultAddress1(String defaultAddress1) {
        this.defaultAddress1 = defaultAddress1;
    }

    public String getDefaultAddress2() {
        return defaultAddress2;
    }

    public void setDefaultAddress2(String defaultAddress2) {
        this.defaultAddress2 = defaultAddress2;
    }

    public String getDefaultCity() {
        return defaultCity;
    }

    public void setDefaultCity(String defaultCity) {
        this.defaultCity = defaultCity;
    }

    public String getDefaultState() {
        return defaultState;
    }

    public void setDefaultState(String defaultState) {
        this.defaultState = defaultState;
    }

    public String getDefaultPostcode() {
        return defaultPostcode;
    }

    public void setDefaultPostcode(String defaultPostcode) {
        this.defaultPostcode = defaultPostcode;
    }

    public String getDefaultCountry() {
        return defaultCountry;
    }

    public void setDefaultCountry(String defaultCountry) {
        this.defaultCountry = defaultCountry;
    }

    public String getDefaultOther() {
        return defaultOther;
    }

    public void setDefaultOther(String defaultOther) {
        this.defaultOther = defaultOther;
    }

    public String getDefaultPhone() {
        return defaultPhone;
    }

    public void setDefaultPhone(String defaultPhone) {
        this.defaultPhone = defaultPhone;
    }

    public String getNameValidationSuccessfullySignUp() {
        return nameValidationSuccessfullySignUp;
    }

    public void setNameValidationSuccessfullySignUp(String nameValidationSuccessfullySignUp) {
        this.nameValidationSuccessfullySignUp = nameValidationSuccessfullySignUp;
    }

    public String getIdHoveredPrinted() {
        return idHoveredPrinted;
    }

    public void setIdHoveredPrinted(String idHoveredPrinted) {
        this.idHoveredPrinted = idHoveredPrinted;
    }

    public String getIdMorePrinted() {
        return idMorePrinted;
    }

    public void setIdMorePrinted(String idMorePrinted) {
        this.idMorePrinted = idMorePrinted;
    }

    public String getIdUniformGroup() {
        return idUniformGroup;
    }

    public void setIdUniformGroup(String idUniformGroup) {
        this.idUniformGroup = idUniformGroup;
    }

    public String getIdSizeM() {
        return idSizeM;
    }

    public void setIdSizeM(String idSizeM) {
        this.idSizeM = idSizeM;
    }

    public String getIdGreenColor() {
        return idGreenColor;
    }

    public void setIdGreenColor(String idGreenColor) {
        this.idGreenColor = idGreenColor;
    }

    public String getIdAddToCard() {
        return idAddToCard;
    }

    public void setIdAddToCard(String idAddToCard) {
        this.idAddToCard = idAddToCard;
    }

    public String getIdContinueShopping() {
        return idContinueShopping;
    }

    public void setIdContinueShopping(String idContinueShopping) {
        this.idContinueShopping = idContinueShopping;
    }

    public String getIdIconHome() {
        return idIconHome;
    }

    public void setIdIconHome(String idIconHome) {
        this.idIconHome = idIconHome;
    }

    public String getIdHoveredFaded() {
        return idHoveredFaded;
    }

    public void setIdHoveredFaded(String idHoveredFaded) {
        this.idHoveredFaded = idHoveredFaded;
    }

    public String getIdMoreFaded() {
        return idMoreFaded;
    }

    public void setIdMoreFaded(String idMoreFaded) {
        this.idMoreFaded = idMoreFaded;
    }

    public String getIdBlueColor() {
        return idBlueColor;
    }

    public void setIdBlueColor(String idBlueColor) {
        this.idBlueColor = idBlueColor;
    }

    public String getIdHoveredBlouse() {
        return idHoveredBlouse;
    }

    public void setIdHoveredBlouse(String idHoveredBlouse) {
        this.idHoveredBlouse = idHoveredBlouse;
    }

    public String getIdMoreBlouse() {
        return idMoreBlouse;
    }

    public void setIdMoreBlouse(String idMoreBlouse) {
        this.idMoreBlouse = idMoreBlouse;
    }

    public String getIdQuantityWanted() {
        return idQuantityWanted;
    }

    public void setIdQuantityWanted(String idQuantityWanted) {
        this.idQuantityWanted = idQuantityWanted;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getIdButtonOrderCard() {
        return idButtonOrderCard;
    }

    public void setIdButtonOrderCard(String idButtonOrderCard) {
        this.idButtonOrderCard = idButtonOrderCard;
    }

    public String getIdButtonProceedToCheckout1() {
        return idButtonProceedToCheckout1;
    }

    public void setIdButtonProceedToCheckout1(String idButtonProceedToCheckout1) {
        this.idButtonProceedToCheckout1 = idButtonProceedToCheckout1;
    }

    public String getIdSubmitLogin() {
        return idSubmitLogin;
    }

    public void setIdSubmitLogin(String idSubmitLogin) {
        this.idSubmitLogin = idSubmitLogin;
    }

    public String getIdButtonProceedToCheckout3() {
        return idButtonProceedToCheckout3;
    }

    public void setIdButtonProceedToCheckout3(String idButtonProceedToCheckout3) {
        this.idButtonProceedToCheckout3 = idButtonProceedToCheckout3;
    }

    public String getIdCheckTermOfService() {
        return idCheckTermOfService;
    }

    public void setIdCheckTermOfService(String idCheckTermOfService) {
        this.idCheckTermOfService = idCheckTermOfService;
    }

    public String getIdButtonProceedToCheckout4() {
        return idButtonProceedToCheckout4;
    }

    public void setIdButtonProceedToCheckout4(String idButtonProceedToCheckout4) {
        this.idButtonProceedToCheckout4 = idButtonProceedToCheckout4;
    }

    public String getIdPaymentType1() {
        return idPaymentType1;
    }

    public void setIdPaymentType1(String idPaymentType1) {
        this.idPaymentType1 = idPaymentType1;
    }

    public String getIdConfirmMyOrder() {
        return idConfirmMyOrder;
    }

    public void setIdConfirmMyOrder(String idConfirmMyOrder) {
        this.idConfirmMyOrder = idConfirmMyOrder;
    }


    public String getIdEmail() {
        return idEmail;
    }

    public void setIdEmail(String idEmail) {
        this.idEmail = idEmail;
    }

    public String getIdQuickViewFaded() {
        return idQuickViewFaded;
    }

    public void setIdQuickViewFaded(String idQuickViewFaded) {
        this.idQuickViewFaded = idQuickViewFaded;
    }

    public String getIdQuickViewBlouse() {
        return idQuickViewBlouse;
    }

    public void setIdQuickViewBlouse(String idQuickViewBlouse) {
        this.idQuickViewBlouse = idQuickViewBlouse;
    }

    public String getIdQuickViewPrinted() {
        return idQuickViewPrinted;
    }

    public void setIdQuickViewPrinted(String idQuickViewPrinted) {
        this.idQuickViewPrinted = idQuickViewPrinted;
    }

    public String getIdIframe() {
        return idIframe;
    }

    public void setIdIframe(String idIframe) {
        this.idIframe = idIframe;
    }

    public String getIdCategoryDress() {
        return idCategoryDress;
    }

    public void setIdCategoryDress(String idCategoryDress) {
        this.idCategoryDress = idCategoryDress;
    }

    public String getIdSummerDress() {
        return idSummerDress;
    }

    public void setIdSummerDress(String idSummerDress) {
        this.idSummerDress = idSummerDress;
    }

    public String getIdCategoryHoveredPrinted() {
        return idCategoryHoveredPrinted;
    }

    public void setIdCategoryHoveredPrinted(String idCategoryHoveredPrinted) {
        this.idCategoryHoveredPrinted = idCategoryHoveredPrinted;
    }

    public String getIdCategoryMorePrinted() {
        return idCategoryMorePrinted;
    }

    public void setIdCategoryMorePrinted(String idCategoryMorePrinted) {
        this.idCategoryMorePrinted = idCategoryMorePrinted;
    }

    public String getIdCategoryWomen() {
        return idCategoryWomen;
    }

    public void setIdCategoryWomen(String idCategoryWomen) {
        this.idCategoryWomen = idCategoryWomen;
    }

    public String getIdSelectTshirt() {
        return idSelectTshirt;
    }

    public void setIdSelectTshirt(String idSelectTshirt) {
        this.idSelectTshirt = idSelectTshirt;
    }

    public String getIdCategoryHoveredFaded() {
        return idCategoryHoveredFaded;
    }

    public void setIdCategoryHoveredFaded(String idCategoryHoveredFaded) {
        this.idCategoryHoveredFaded = idCategoryHoveredFaded;
    }

    public String getIdHoveredColorFaded() {
        return idHoveredColorFaded;
    }

    public void setIdHoveredColorFaded(String idHoveredColorFaded) {
        this.idHoveredColorFaded = idHoveredColorFaded;
    }

    public String getIdSelectBlouse() {
        return idSelectBlouse;
    }

    public void setIdSelectBlouse(String idSelectBlouse) {
        this.idSelectBlouse = idSelectBlouse;
    }

    public String getIdCategoryHoveredBlouse() {
        return idCategoryHoveredBlouse;
    }

    public void setIdCategoryHoveredBlouse(String idCategoryHoveredBlouse) {
        this.idCategoryHoveredBlouse = idCategoryHoveredBlouse;
    }

    public String getIdCategoryQuickViewBlouse() {
        return idCategoryQuickViewBlouse;
    }

    public void setIdCategoryQuickViewBlouse(String idCategoryQuickViewBlouse) {
        this.idCategoryQuickViewBlouse = idCategoryQuickViewBlouse;
    }

    public String getIdEveningDress() {
        return idEveningDress;
    }

    public void setIdEveningDress(String idEveningDress) {
        this.idEveningDress = idEveningDress;
    }

    public String getIdCategoryHoveredPrinted2() {
        return idCategoryHoveredPrinted2;
    }

    public void setIdCategoryHoveredPrinted2(String idCategoryHoveredPrinted2) {
        this.idCategoryHoveredPrinted2 = idCategoryHoveredPrinted2;
    }

    public String getIdQuickAddToCardPrinted2() {
        return idQuickAddToCardPrinted2;
    }

    public void setIdQuickAddToCardPrinted2(String idQuickAddToCardPrinted2) {
        this.idQuickAddToCardPrinted2 = idQuickAddToCardPrinted2;
    }
    public String getDefultTitleHomePage() {
        return defultTitleHomePage;
    }

    public void setDefultTitleHomePage(String defultTitleHomePage) {
        this.defultTitleHomePage = defultTitleHomePage;
    }

    public String getColor01() {
        return color01;
    }

    public void setColor01(String color01) {
        this.color01 = color01;
    }

    public String getQuantityWanted() {
        return quantityWanted;
    }

    public void setQuantityWanted(String quantityWanted) {
        this.quantityWanted = quantityWanted;
    }

    public String getValidation1() {
        return validation1;
    }

    public void setValidation1(String validation1) {
        this.validation1 = validation1;
    }

    public String getValidation2() {
        return validation2;
    }

    public void setValidation2(String validation2) {
        this.validation2 = validation2;
    }

    public String getValidation3() {
        return validation3;
    }

    public void setValidation3(String validation3) {
        this.validation3 = validation3;
    }

    public String getValidation4() {
        return validation4;
    }

    public void setValidation4(String validation4) {
        this.validation4 = validation4;
    }

    public String getValidation5() {
        return validation5;
    }

    public void setValidation5(String validation5) {
        this.validation5 = validation5;
    }

    public String getValidation6() {
        return validation6;
    }

    public void setValidation6(String validation6) {
        this.validation6 = validation6;
    }

    public String getDefaultEmail() {
        return defaultEmail;
    }

    public void setDefaultEmail(String defaultEmail) {
        this.defaultEmail = defaultEmail;
    }

    public String getDefaultPassword() {
        return defaultPassword;
    }

    public void setDefaultPassword(String defaultPassword) {
        this.defaultPassword = defaultPassword;
    }

    public String getDefaultMensageOrderCompleted() {
        return defaultMensageOrderCompleted;
    }

    public void setDefaultMensageOrderCompleted(String defaultMensageOrderCompleted) {
        this.defaultMensageOrderCompleted = defaultMensageOrderCompleted;
    }
}
