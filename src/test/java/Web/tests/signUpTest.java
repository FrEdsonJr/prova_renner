package Web.tests;

import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import Web.resources.variables;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class signUpTest {

    variables variables = new variables();
    WebDriver driver = new ChromeDriver();
    WebDriverWait wait = new WebDriverWait(driver, 10);
    Actions builder = new Actions(driver);
    JavascriptExecutor js= (JavascriptExecutor)driver;

    @Before
    public void OpenBrowser(){
        // Configurações
        // Abrindo Navegador
        System.setProperty("Webdriver.chrome.driver", variables.getArquivoChromeDriver());

        Dimension d = new Dimension(1920,1080);

        JavascriptExecutor js= (JavascriptExecutor)driver;

        // Inicializando Página
        driver.manage().window().setSize(d);
        driver.get(variables.getBaseURL());
    }
    @After
    public void CloseBrowser(){
        driver.quit();
    }
    @Test
    public void testSuccessfullySignUp() throws InterruptedException {       // Cenário: Realizar cadastro de um novo usuário;

        // Cadastro de usuário

        // Entrar na tela de cadastro

        Thread.sleep(500);
        driver.findElement(By.xpath(variables.getIdButtonSignUp())).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(variables.getIdEmailCreate())));
        driver.findElement(By.id(variables.getIdEmailCreate())).sendKeys(variables.getFakeFirstName() + "@me.com");
        driver.findElement(By.id(variables.getIdSubmitCreate())).click();

        // Input de Dados Pessoais

        Thread.sleep(400);
        js.executeScript("window.scrollBy(0,400)", "");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(variables.getIdGender())));
        driver.findElement(By.id(variables.getIdGender())).click();
        driver.findElement(By.id(variables.getIdCustomerFirstname())).sendKeys(variables.getFakeFirstName());
        driver.findElement(By.id(variables.getIdCustomerLastname())).sendKeys(variables.getFakeLastName());
        driver.findElement(By.id(variables.getIdPasswd())).sendKeys(variables.getDefaulPassword());
        driver.findElement(By.id(variables.getIdDays())).click();
        Select days = new Select(driver.findElement(By.id(variables.getIdDays())));
        days.selectByValue(variables.getDefaultDay());
        driver.findElement(By.id(variables.getIdMonths())).click();
        Select months = new Select(driver.findElement(By.id(variables.getIdMonths())));
        months.selectByValue(variables.getDefaultMonth());
        driver.findElement(By.id(variables.getIdYears())).click();
        Select years = new Select(driver.findElement(By.id(variables.getIdYears())));
        years.selectByValue(variables.getDefaultYear());
        driver.findElement(By.id(variables.getIdNewsletter())).click();
        driver.findElement(By.id(variables.getIdOptin())).click();
        js.executeScript("window.scrollBy(0,400)", "");

        // Input de Endereço

        driver.findElement(By.id(variables.getIdAddressFirstname())).sendKeys(variables.getFakeFirstName());
        driver.findElement(By.id(variables.getIdAddressLastname())).sendKeys(variables.getFakeLastName());
        driver.findElement(By.id(variables.getIdCompany())).sendKeys(variables.getDefaultCompany());
        driver.findElement(By.id(variables.getIdAddress1())).sendKeys(variables.getDefaultAddress1());
        driver.findElement(By.id(variables.getIdAddress2())).sendKeys(variables.getDefaultAddress2());
        driver.findElement(By.id(variables.getIdCity())).sendKeys(variables.getDefaultCity());
        driver.findElement(By.id(variables.getIdState())).click();
        Select id_state = new Select(driver.findElement(By.id(variables.getIdState())));
        id_state.selectByValue(variables.getDefaultState());
        driver.findElement(By.id(variables.getIdPostcode())).sendKeys(variables.getDefaultPostcode());
        driver.findElement(By.id(variables.getIdCountry())).click();
        Select id_country = new Select(driver.findElement(By.id(variables.getIdCountry())));
        id_country.selectByValue(variables.getDefaultCountry());
        js.executeScript("window.scrollBy(0,400)", "");
        driver.findElement(By.id(variables.getIdOther())).sendKeys(variables.getDefaultOther());
        driver.findElement(By.id(variables.getIdPhone())).sendKeys(variables.getDefaultPhone());
        driver.findElement(By.id(variables.getIdPhoneMobile())).sendKeys(variables.getFakePhoneNumber());
        driver.findElement(By.id(variables.getIdAlias())).sendKeys(variables.getFakeLastName() + "@me.com");
        driver.findElement(By.id(variables.getIdSubmitAccount())).click();

        // Validando se está na página pós cadastro

        driver.getPageSource().contains(variables.getNameValidationSuccessfullySignUp());
    }
}
