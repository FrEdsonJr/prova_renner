package Web.tests;

import Web.resources.variables;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class purchasesTest {

    Web.resources.variables variables = new variables();
    WebDriver driver = new ChromeDriver();
    WebDriverWait wait = new WebDriverWait(driver, 10);
    Actions builder = new Actions(driver);
    JavascriptExecutor js= (JavascriptExecutor)driver;

    @Before
    public void OpenBrowser(){
        // Configurações
        // Abrindo Navegador
        System.setProperty("Webdriver.chrome.driver", variables.getArquivoChromeDriver());

        Dimension d = new Dimension(1920,1080);

        JavascriptExecutor js= (JavascriptExecutor)driver;

        // Inicializando Página
        driver.manage().window().setSize(d);
        driver.get(variables.getBaseURL());
    }
    @After
    public void CloseBrowser(){
        driver.quit();
    }

    @Test
    public void testPurchasesWithMore() throws InterruptedException {       // Cenário: Realizar compra de 3 produtos por meio da opção "More" (Logando na tela de carrinho);
        // Compra da "Printed Chiffon Dress" na cor verde e tamanho "M"

        Thread.sleep(500);
        js.executeScript("window.scrollBy(0,850)", "");
        WebElement element_Printed = driver.findElement(By.xpath(variables.getIdHoveredPrinted()));
        builder.moveToElement(element_Printed).build().perform();
        driver.findElement(By.xpath(variables.getIdMorePrinted())).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(variables.getIdUniformGroup())));
        js.executeScript("window.scrollBy(0,350)", "");
        driver.findElement(By.id(variables.getIdUniformGroup())).click();
        driver.findElement(By.xpath(variables.getIdSizeM())).click();
        driver.findElement(By.id(variables.getIdGreenColor())).click();
        driver.findElement(By.id(variables.getIdAddToCard())).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(variables.getIdContinueShopping())));
        driver.findElement(By.xpath(variables.getIdContinueShopping())).click();
        js.executeScript("window.scrollBy(0,-350)", "");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(variables.getIdIconHome())));
        driver.findElement(By.xpath(variables.getIdIconHome())).click();
        driver.getPageSource().contains(variables.getDefultTitleHomePage());

        // Compra da "Faded Short Sleeve" T-shirts na cor azul

        Thread.sleep(500);
        js.executeScript("window.scrollBy(0,850)", "");
        WebElement element_Faded = driver.findElement(By.xpath(variables.getIdHoveredFaded()));
        builder.moveToElement(element_Faded).build().perform();
        driver.findElement(By.xpath(variables.getIdMoreFaded())).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(variables.getIdBlueColor())));
        js.executeScript("window.scrollBy(0,350)", "");
        driver.findElement(By.id(variables.getIdBlueColor())).click();
        driver.findElement(By.id(variables.getIdAddToCard())).click();
        driver.getPageSource().contains("Blue");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(variables.getIdContinueShopping())));
        driver.findElement(By.xpath(variables.getIdContinueShopping())).click();
        js.executeScript("window.scrollBy(0,-350)", "");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(variables.getIdIconHome())));
        driver.findElement(By.xpath(variables.getIdIconHome())).click();
        driver.getPageSource().contains(variables.getDefultTitleHomePage());


        // Compra da "Blouse" na quantidade 2

        Thread.sleep(500);
        js.executeScript("window.scrollBy(0,850)", "");
        WebElement element_Blouse = driver.findElement(By.xpath(variables.getIdHoveredBlouse()));
        builder.moveToElement(element_Blouse).build().perform();
        driver.findElement(By.xpath(variables.getIdMoreBlouse())).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(variables.getIdQuantityWanted())));
        js.executeScript("window.scrollBy(0,350)", "");
        driver.findElement(By.xpath(variables.getIdQuantityWanted())).click();
        driver.findElement(By.id(variables.getIdAddToCard())).click();
        driver.getPageSource().contains("2");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(variables.getIdContinueShopping())));
        driver.findElement(By.xpath(variables.getIdContinueShopping())).click();
        js.executeScript("window.scrollBy(0,-350)", "");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(variables.getIdIconHome())));
        driver.findElement(By.xpath(variables.getIdIconHome())).click();
        driver.getPageSource().contains(variables.getDefultTitleHomePage());


        // Finalizar compra

        // Etapa 01 - Validando itens no carrinho de compra

        Thread.sleep(500);
        js.executeScript("window.scrollBy(0,400)", "");
        WebElement element_card = driver.findElement(By.xpath(variables.getIdCard()));
        builder.moveToElement(element_card).build().perform();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(variables.getIdButtonOrderCard())));
        driver.findElement(By.id(variables.getIdButtonOrderCard())).click();
        js.executeScript("window.scrollBy(0,450)", "");
        driver.getPageSource().contains(variables.getValidation1());
        driver.getPageSource().contains(variables.getColor01());
        driver.getPageSource().contains(variables.getValidation3());
        driver.getPageSource().contains(variables.getValidation4());
        driver.findElement(By.xpath(variables.getIdButtonProceedToCheckout1())).click();

        // Etapa 02 - Login

        Thread.sleep(500);
        js.executeScript("window.scrollBy(0,400)", "");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(variables.getIdEmail())));
        driver.findElement(By.id(variables.getIdEmail())).sendKeys("francisco@me.com");
        driver.findElement(By.id(variables.getIdPasswd())).sendKeys("12345678");
        driver.findElement(By.id(variables.getIdSubmitLogin())).click();

        // Etapa 03 - Endereço

        Thread.sleep(500);
        js.executeScript("window.scrollBy(0,400)", "");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(variables.getIdButtonProceedToCheckout3())));
        driver.findElement(By.xpath(variables.getIdButtonProceedToCheckout3())).click();

        // Etapa 04 - Frete

        Thread.sleep(500);
        js.executeScript("window.scrollBy(0,400)", "");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(variables.getIdCheckTermOfService())));
        driver.findElement(By.id(variables.getIdCheckTermOfService())).click();
        driver.findElement(By.xpath(variables.getIdButtonProceedToCheckout4())).click();

        // Etapa 05 - Pagamento

        Thread.sleep(500);
        js.executeScript("window.scrollBy(0,400)", "");
        driver.findElement(By.xpath(variables.getIdPaymentType1())).click();
        Thread.sleep(500);
        driver.findElement(By.xpath(variables.getIdConfirmMyOrder())).click();
        Thread.sleep(500);
        driver.getPageSource().contains("Your order on My Store is complete.");
    }
    @Test
    public void testPurchasesWithQuickView() throws InterruptedException {       // Cenário: Realizar compra de 3 produtos por meio da opção "Quick View" (Logando na tela de carrinho);
        // Compra da "Printed Chiffon Dress" na cor verde e tamanho "M"

        Thread.sleep(500);
        js.executeScript("window.scrollBy(0,850)", "");
        WebElement element_Printed = driver.findElement(By.xpath(variables.getIdHoveredPrinted()));
        builder.moveToElement(element_Printed).build().perform();
        driver.findElement(By.xpath(variables.getIdQuickViewPrinted())).click();
        Thread.sleep(2500);
        WebElement iframeElement_Printed = driver.findElement(By.xpath(variables.getIdIframe()));
        driver.switchTo().frame(iframeElement_Printed);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(variables.getIdUniformGroup())));
        driver.findElement(By.id(variables.getIdUniformGroup())).click();
        driver.findElement(By.xpath(variables.getIdSizeM())).click();
        driver.findElement(By.id(variables.getIdGreenColor())).click();
        driver.findElement(By.id(variables.getIdAddToCard())).click();
        driver.switchTo().defaultContent();
        driver.getPageSource().contains("Green, M");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(variables.getIdContinueShopping())));
        driver.findElement(By.xpath(variables.getIdContinueShopping())).click();
        driver.getPageSource().contains(variables.getDefultTitleHomePage());

        // Compra da "Faded Short Sleeve" T-shirts na cor azul

        Thread.sleep(500);
        WebElement element_Faded = driver.findElement(By.xpath(variables.getIdHoveredFaded()));
        builder.moveToElement(element_Faded).build().perform();
        driver.findElement(By.xpath(variables.getIdQuickViewFaded())).click();
        Thread.sleep(2500);
        WebElement iframeElement_Faded = driver.findElement(By.xpath(variables.getIdIframe()));
        driver.switchTo().frame(iframeElement_Faded);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(variables.getIdBlueColor())));
        js.executeScript("window.scrollBy(0,350)", "");
        driver.findElement(By.id(variables.getIdBlueColor())).click();
        driver.findElement(By.id(variables.getIdAddToCard())).click();
        driver.switchTo().defaultContent();
        driver.getPageSource().contains("Blue");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(variables.getIdContinueShopping())));
        driver.findElement(By.xpath(variables.getIdContinueShopping())).click();
        driver.getPageSource().contains(variables.getDefultTitleHomePage());

        // Compra da "Blouse" na quantidade 2

        Thread.sleep(500);
        WebElement element_Blouse = driver.findElement(By.xpath(variables.getIdHoveredBlouse()));
        builder.moveToElement(element_Blouse).build().perform();
        driver.findElement(By.xpath(variables.getIdQuickViewBlouse())).click();
        Thread.sleep(2500);
        WebElement iframeElement_Blouse = driver.findElement(By.xpath(variables.getIdIframe()));
        driver.switchTo().frame(iframeElement_Blouse);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(variables.getIdQuantityWanted())));
        js.executeScript("window.scrollBy(0,350)", "");
        driver.findElement(By.xpath(variables.getIdQuantityWanted())).click();
        driver.findElement(By.id(variables.getIdAddToCard())).click();
        driver.switchTo().defaultContent();
        driver.getPageSource().contains(variables.getValidation3()); // a forma que foi validado esse campo, não foi a mais eficiente, mas a validação foi feita.
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(variables.getIdContinueShopping())));
        driver.findElement(By.xpath(variables.getIdContinueShopping())).click();
        driver.getPageSource().contains(variables.getDefultTitleHomePage());

        // Finalizar compra

        // Etapa 01 - Validando itens no carrinho de compra

        Thread.sleep(500);
        js.executeScript("window.scrollBy(0,400)", "");
        WebElement element_card = driver.findElement(By.xpath(variables.getIdCard()));
        builder.moveToElement(element_card).build().perform();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(variables.getIdButtonOrderCard())));
        driver.findElement(By.id(variables.getIdButtonOrderCard())).click();
        js.executeScript("window.scrollBy(0,450)", "");
        driver.getPageSource().contains(variables.getValidation1());
        driver.getPageSource().contains(variables.getColor01());
        driver.getPageSource().contains(variables.getValidation3());
        driver.getPageSource().contains(variables.getValidation4());
        driver.findElement(By.xpath(variables.getIdButtonProceedToCheckout1())).click();

        // Etapa 02 - Login

        Thread.sleep(500);
        js.executeScript("window.scrollBy(0,400)", "");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(variables.getIdEmail())));
        driver.findElement(By.id(variables.getIdEmail())).sendKeys(variables.getDefaultEmail());
        driver.findElement(By.id(variables.getIdPasswd())).sendKeys(variables.getDefaultPassword());
        driver.findElement(By.id(variables.getIdSubmitLogin())).click();

        // Etapa 03 - Endereço

        Thread.sleep(500);
        js.executeScript("window.scrollBy(0,400)", "");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(variables.getIdButtonProceedToCheckout3())));
        driver.findElement(By.xpath(variables.getIdButtonProceedToCheckout3())).click();

        // Etapa 04 - Frete

        Thread.sleep(500);
        js.executeScript("window.scrollBy(0,400)", "");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(variables.getIdCheckTermOfService())));
        driver.findElement(By.id(variables.getIdCheckTermOfService())).click();
        driver.findElement(By.xpath(variables.getIdButtonProceedToCheckout4())).click();

        // Etapa 05 - Pagamento

        Thread.sleep(500);
        js.executeScript("window.scrollBy(0,400)", "");
        driver.findElement(By.xpath(variables.getIdPaymentType1())).click();
        Thread.sleep(500);
        driver.findElement(By.xpath(variables.getIdConfirmMyOrder())).click();
        Thread.sleep(500);
        driver.getPageSource().contains(variables.getDefaultMensageOrderCompleted());
    }
    @Test
    public void testPurchasesWithCategories() throws InterruptedException {       // Cenário: Realizar compra de 4 produtos por categorias (Já logado no sistema);
        // Login

        Thread.sleep(500);
        driver.findElement(By.xpath(variables.getIdButtonSignUp())).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(variables.getIdEmail())));
        driver.findElement(By.id(variables.getIdEmail())).sendKeys(variables.getDefaultEmail());
        driver.findElement(By.id(variables.getIdPasswd())).sendKeys(variables.getDefaultPassword());
        driver.findElement(By.id(variables.getIdSubmitLogin())).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(variables.getIdIconHome())));
        driver.findElement(By.xpath(variables.getIdIconHome())).click();
        driver.getPageSource().contains(variables.getDefultTitleHomePage());

        // Compra da "Printed Chiffon Dress" na cor verde e tamanho "M"

        Thread.sleep(500);

        WebElement element_Printed_1 = driver.findElement(By.xpath(variables.getIdCategoryDress()));
        builder.moveToElement(element_Printed_1).build().perform();
        driver.findElement(By.xpath(variables.getIdSummerDress())).click();
        Thread.sleep(1500);
        js.executeScript("window.scrollBy(0,700)", "");
        WebElement element_Printed_2 = driver.findElement(By.xpath(variables.getIdCategoryHoveredPrinted()));
        builder.moveToElement(element_Printed_2).build().perform();
        driver.findElement(By.xpath(variables.getIdCategoryMorePrinted())).click();
        js.executeScript("window.scrollBy(0,300)", "");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(variables.getIdUniformGroup())));
        driver.findElement(By.xpath(variables.getIdSizeM())).click();
        driver.findElement(By.id(variables.getIdGreenColor())).click();
        driver.findElement(By.id(variables.getIdAddToCard())).click();
        driver.switchTo().defaultContent();
        driver.getPageSource().contains(variables.getValidation5());
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(variables.getIdContinueShopping())));
        driver.findElement(By.xpath(variables.getIdContinueShopping())).click();
        js.executeScript("window.scrollBy(0,-350)", "");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(variables.getIdIconHome())));
        driver.findElement(By.xpath(variables.getIdIconHome())).click();
        driver.getPageSource().contains(variables.getDefultTitleHomePage());

        // Compra da "Faded Short Sleeve" T-shirts na cor azul

        Thread.sleep(500);
        WebElement element_Faded_1 = driver.findElement(By.xpath(variables.getIdCategoryWomen()));
        builder.moveToElement(element_Faded_1).build().perform();
        driver.findElement(By.xpath(variables.getIdSelectTshirt())).click();
        Thread.sleep(1500);
        js.executeScript("window.scrollBy(0,500)", "");
        WebElement element_Faded_2 = driver.findElement(By.xpath(variables.getIdCategoryHoveredFaded()));
        builder.moveToElement(element_Faded_2).build().perform();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(variables.getIdHoveredColorFaded())));
        driver.findElement(By.id(variables.getIdHoveredColorFaded())).click();
        Thread.sleep(1500);
        js.executeScript("window.scrollBy(0,350)", "");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(variables.getIdAddToCard())));
        driver.findElement(By.id(variables.getIdAddToCard())).click();
        driver.switchTo().defaultContent();
        driver.getPageSource().contains(variables.getColor01());
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(variables.getIdContinueShopping())));
        driver.findElement(By.xpath(variables.getIdContinueShopping())).click();
        js.executeScript("window.scrollBy(0,-350)", "");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(variables.getIdIconHome())));
        driver.findElement(By.xpath(variables.getIdIconHome())).click();
        driver.getPageSource().contains(variables.getDefultTitleHomePage());

        // Compra da "Blouse" na quantidade 2

        Thread.sleep(500);
        WebElement element_Blouse_1 = driver.findElement(By.xpath(variables.getIdCategoryWomen()));
        builder.moveToElement(element_Blouse_1).build().perform();
        driver.findElement(By.xpath(variables.getIdSelectBlouse())).click();
        Thread.sleep(1500);
        js.executeScript("window.scrollBy(0,500)", "");
        WebElement element_Blouse_2 = driver.findElement(By.xpath(variables.getIdCategoryHoveredBlouse()));
        builder.moveToElement(element_Blouse_2).build().perform();
        driver.findElement(By.xpath(variables.getIdCategoryQuickViewBlouse())).click();
        Thread.sleep(1500);
        WebElement iframeElement_Blouse = driver.findElement(By.xpath(variables.getIdIframe()));
        driver.switchTo().frame(iframeElement_Blouse);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(variables.getIdQuantityWanted())));
        js.executeScript("window.scrollBy(0,350)", "");
        driver.findElement(By.xpath(variables.getIdQuantityWanted())).click();
        driver.findElement(By.id(variables.getIdAddToCard())).click();
        driver.switchTo().defaultContent();
        driver.getPageSource().contains("2"); // a forma que foi validado esse campo, não foi a mais eficiente, mas a validação foi feita.
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(variables.getIdContinueShopping())));
        driver.findElement(By.xpath(variables.getIdContinueShopping())).click();
        js.executeScript("window.scrollBy(0,-350)", "");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(variables.getIdIconHome())));
        driver.findElement(By.xpath(variables.getIdIconHome())).click();
        driver.getPageSource().contains(variables.getDefultTitleHomePage());

        // Compra da "Printed Dress"

        Thread.sleep(500);
        WebElement element_Printed_3 = driver.findElement(By.xpath(variables.getIdCategoryDress()));
        builder.moveToElement(element_Printed_3).build().perform();
        driver.findElement(By.xpath(variables.getIdEveningDress())).click();
        Thread.sleep(1500);
        js.executeScript("window.scrollBy(0,700)", "");
        WebElement element_Printed_4 = driver.findElement(By.xpath(variables.getIdCategoryHoveredPrinted2()));
        builder.moveToElement(element_Printed_4).build().perform();
        driver.findElement(By.xpath(variables.getIdQuickAddToCardPrinted2())).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(variables.getIdContinueShopping())));
        driver.findElement(By.xpath(variables.getIdContinueShopping())).click();
        js.executeScript("window.scrollBy(0,-350)", "");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(variables.getIdIconHome())));
        driver.findElement(By.xpath(variables.getIdIconHome())).click();
        driver.getPageSource().contains(variables.getDefultTitleHomePage());

        // Finalizar compra

        // Etapa 01 - Validando itens no carrinho de compra

        Thread.sleep(500);
        js.executeScript("window.scrollBy(0,400)", "");
        WebElement element_card = driver.findElement(By.xpath(variables.getIdCard()));
        builder.moveToElement(element_card).build().perform();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(variables.getIdButtonOrderCard())));
        driver.findElement(By.id(variables.getIdButtonOrderCard())).click();
        js.executeScript("window.scrollBy(0,450)", "");
        driver.getPageSource().contains(variables.getValidation1());
        driver.getPageSource().contains(variables.getColor01());
        driver.getPageSource().contains(variables.getValidation3());
        driver.getPageSource().contains(variables.getValidation6());
        driver.findElement(By.xpath(variables.getIdButtonProceedToCheckout1())).click();

        // Etapa 03 - Endereço

        Thread.sleep(500);
        js.executeScript("window.scrollBy(0,400)", "");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(variables.getIdButtonProceedToCheckout3())));
        driver.findElement(By.xpath(variables.getIdButtonProceedToCheckout3())).click();

        // Etapa 04 - Frete

        Thread.sleep(500);
        js.executeScript("window.scrollBy(0,400)", "");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(variables.getIdCheckTermOfService())));
        driver.findElement(By.id(variables.getIdCheckTermOfService())).click();
        driver.findElement(By.xpath(variables.getIdButtonProceedToCheckout4())).click();

        // Etapa 05 - Pagamento

        Thread.sleep(500);
        js.executeScript("window.scrollBy(0,400)", "");
        driver.findElement(By.xpath(variables.getIdPaymentType1())).click();
        Thread.sleep(500);
        driver.findElement(By.xpath(variables.getIdConfirmMyOrder())).click();
        Thread.sleep(500);
        driver.getPageSource().contains(variables.getDefaultMensageOrderCompleted());
    }

}