# Prova Técnica: Automação de Teste (Renner)
_**Prova consiste no desenvolvimento de duas automações: Web e API.**_ 
___
 Web
-
Desenvolva, uma automação de testes para o e-commerce: http://automationpractice.com/. 
* _O projeto deve conter, pelo menos, um cenário de teste para a seguinte História do Usuário:_

### História de Usuário

**Como** um cliente cadastrado no automationpractice.com <br />
**Eu quero** fazer a compra de ao menos três produtos <br />
**Para** que eu possa estar bem vestida 

### Produtos:

- Printed Chiffon Dress na cor verde e tamanho "M"
- Faded Short Sleeve T-shirts na cor azul
- Blouse na quantidade 2
- Printed Dress
___

### Cenários:

- Realizar compra de 3 produtos por meio da opção **"More"**  (Logando na tela de carrinho);
- Realizar compra de 3 produtos por meio da opção **"Quick View"**  (Logando na tela de carrinho);
- Realizar compra de 4 produtos por categorias (Já logado no sistema);

### Cenário Bônus:

- Realizar cadastro de um novo usuário;
---

API
-
Desenvolva uma automação de testes para a API cuja documentação está descrita no site: https://reqres.in/.
* _O projeto deve conter ao menos um cenário para cada um dos seguintes métodos do endpoint USERS:_

### Métodos:

- POST
- GET SINGLE USER
- GET LIST USERS
- PATCH

### Cenários:

- Cadastro de usuário [POST]
- Pesquisa de um usuário [GET]

### Cenário Bônus:

- Listagem de usuários por página [GET]
- Data e hora de atualização de usuário [PATCH]

---

Como executar?
-

1. **Baixar o repositório: </br>**
`git clone https://gitlab.com/FrEdsonJr/prova_renner.git`
2. **Rodar repositório em uma IDE:**
     - Ex.:
       - Eclipse
       - intellij
3. **Instalar as dependências _(Encontram-se no arquivo "pom.xml")_**
   - junit
   - org.seleniumhq.selenium
   - com.github.javafaker
   - io.rest-assured 
4. **Instalar "ChromeDriver":**
    - Link: https://chromedriver.chromium.org/downloads
5. **Alterar diretório no arquivo**
      - /src/test/java/Web/resources/variables.java
      - linha 11
        - Deve ser iniserido o caminho do chromedriver instalado na sua máquina.

* Após realizar essas etapas, apenas rode o projeto na sua máquina.